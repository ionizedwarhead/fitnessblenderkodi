import sys
import requests
from bs4 import BeautifulSoup

import xbmcgui
import xbmcplugin

import util

def playVideo(params):
    pass

def buildMenu():
    url = WEB_PAGE_BASE + VIDEO_DIR
    request = requests.get(url)
    response = BeautifulSoup(request.text)

    #Test code to get a video up on the page
    if request and request.status_code == 200:
        for topPage in response.select("a.videolink"):
            videoImg = topPage.div.find_next_sibling("div").img
            videoTitle = videoImg['alt']
            url2 = WEB_PAGE_BASE + topPage['href']
            request2 = requests.get(url2)
            response2 = BeautifulSoup(request2.text)
            if request and request.status_code == 200:
                videoUrl = response2.find("iframe")['src']
                li = xbmcgui.ListItem(videoTitle, iconImage=WEB_PAGE_BASE+videoImg['src'])
                xbmcplugin.addDirectoryItem(handle=addon_handle, url=videoUrl, listitem=li)

    else:
        util.showError(addon_handle, 'Count not open URL %s to create menu' % (url))



WEB_PAGE_BASE = 'http://www.fitnessblender.com'
VIDEO_DIR = '/videos'
addon_handle = int(sys.argv[1])
xbmcplugin.setContent(addon_handle, 'movies')

parameters = util.parseParameters()
if 'play' in parameters:
    playVideo(parameters['play'])
else:
    buildMenu()

# addon_handle = int(sys.argv[1])
#
# xbmcplugin.setContent(addon_handle, 'movies')
#
# url = 'http://localhost/some_video.mkv'
# li = xbmcgui.ListItem('My First Video', iconImage='DefaultVideo.png')
# xbmcplugin.addDirectoryItem(handle=addon_handle, url=url, listitem=li)
#
xbmcplugin.endOfDirectory(addon_handle)