import sys

#import xbmcgui
#import xbmcplugin
import xbmc
import xbmcaddon

import urllib

def parseParameters(inputString=sys.argv[2]):
    parameters = {}
    p1 = inputString.find('?')
    if p1 >= 0:
        splitParameters = inputString[p1 + 1:].split('&')
        for nameValuePair in splitParameters:
            if (len(nameValuePair) > 0):
                pair = nameValuePair.split('=')
                key = pair[0]
                value=urllib.unquote(urllib.unquote_plus(pair[1])).decode('utf-8')

    return parameters

def notify(addonId, message, timeShown=5000):
    addon = xbmcaddon.Addon(addonId)
    xbmc.executebuiltin('Notification(%s, %s, %d, %s)') % (addon.getAddonInfo('name'), message, timeShown, addon.getAddonInfo('icon'))

def showError(addonId, errorMessage):
    notify(addonId, errorMessage)
    xbmc.log(errorMessage, xbmc.LOGERROR)